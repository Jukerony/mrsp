﻿using System.Windows;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain.ConcretHandlers
{
    public class OkHandler : RegisterResultHandler
    {
        public override void Handle(MailRegistrationResult receiver)
        {
            if (receiver == MailRegistrationResult.Ok)
                MessageBox.Show("Письмо успешно зарегестрировано", "Notify", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            else
            {
                Successor?.Handle(receiver);
            }
        }
    }
}
