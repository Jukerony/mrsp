﻿using System.Windows;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain.ConcretHandlers
{
    public class DataBaseConnectFaildHandler : RegisterResultHandler
    {
        public override void Handle(MailRegistrationResult receiver)
        {
            if (receiver == MailRegistrationResult.DataBaseConnectFaild)
                MessageBox.Show("Удаленная база данных недоступна", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            else
            {
                Successor?.Handle(receiver);
            }
        }
    }
}
