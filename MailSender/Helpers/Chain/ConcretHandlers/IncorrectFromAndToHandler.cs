﻿using System.Windows;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain.ConcretHandlers
{
    public class IncorrectFromAndToHandler : RegisterResultHandler
    {
        public override void Handle(MailRegistrationResult receiver)
        {
            if (receiver == MailRegistrationResult.IncorrectFromAndTo)
                MessageBox.Show("Отправитель и получатель не являются сотрудниками компании", "Warning",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            else
            {
                Successor?.Handle(receiver);
            }
        }
    }
}
