﻿using System.Windows;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain.ConcretHandlers
{
    public class IncorrectToHandler : RegisterResultHandler
    {
        public override void Handle(MailRegistrationResult receiver)
        {
            if (receiver == MailRegistrationResult.IncorrectTo)
                MessageBox.Show("Получатель не является сотрудником компании", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            else
            {
                Successor?.Handle(receiver);
            }
        }
    }
}
