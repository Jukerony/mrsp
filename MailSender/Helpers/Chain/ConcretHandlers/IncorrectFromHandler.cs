﻿using System.Windows;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain.ConcretHandlers
{
    public class IncorrectFromHandler: RegisterResultHandler
    {
        public override void Handle(MailRegistrationResult receiver)
        {
            if (receiver == MailRegistrationResult.IncorrectFrom)
                MessageBox.Show("Отправитель не является сотрудником компании", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            else
            {
                Successor?.Handle(receiver);
            }
        }
    }
}
