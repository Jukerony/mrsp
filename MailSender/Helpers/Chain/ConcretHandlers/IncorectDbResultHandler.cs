﻿using System.Windows;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain.ConcretHandlers
{
    public class IncorectDbResultHandler : RegisterResultHandler
    {
        public override void Handle(MailRegistrationResult receiver)
        {
            if (receiver == MailRegistrationResult.IncorectDbResult)
                MessageBox.Show("База данных вернула результат отличый от ожидаемого", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            else
            {
                Successor?.Handle(receiver);
            }
        }
    }
}
