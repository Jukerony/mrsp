﻿using MailSender.Helpers.Chain.ConcretHandlers;
using SharedLibrary.Contracts;

namespace MailSender.Helpers.Chain
{
    public abstract class RegisterResultHandler
    {
        protected RegisterResultHandler Successor { get; private set; }
        public abstract void Handle(MailRegistrationResult receiver);

        /// <summary>
        /// Инициализация цепочки обработки
        /// </summary>
        /// <returns></returns>
        public static RegisterResultHandler InitChain()
        {
            var dbConnectHandler = new DataBaseConnectFaildHandler();
            var incrorrectDbResult = new IncorectDbResultHandler();
            var incorrectFromAndToResult = new IncorrectFromAndToHandler();
            var incorrectFromResult = new IncorrectFromHandler();
            var incorrectToResult = new IncorrectToHandler();
            var validationResult = new ValidationFaildHandler();
            var okResult = new OkHandler();

            okResult.Successor = incorrectFromResult;
            incorrectFromResult.Successor = incorrectToResult;
            incorrectToResult.Successor =  incorrectFromAndToResult;
            incorrectFromAndToResult.Successor = validationResult;
            validationResult.Successor = dbConnectHandler;
            dbConnectHandler.Successor = incrorrectDbResult;
            return okResult;
        }
    }
}
