﻿using System.Net;
using System.ServiceModel;
using SharedLibrary.Contracts;
using System.Windows;
using MailSender.DbAcsess;
using MailSender.Helpers.Chain;

namespace MailSender
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly RegisterResultHandler _handler = RegisterResultHandler.InitChain();
        private readonly Mail _workMail;
        public MainWindow()
        {
            InitializeComponent();
            _workMail = new Mail();
            DataContext = _workMail;
        }
        private async void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (_workMail.IsValid())
            {
                SendButton.IsEnabled = false;
                try
                {
                    using (var client = new DbAcsessServiceClient())
                    {
                        var result = await client.RegisterMailAsync(_workMail);
                        _handler.Handle(result);
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Ошибка при подключении к удаленному сервису", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch(CommunicationException)
                {
                    MessageBox.Show("Ошибка при общении с удаленным сервисом", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                SendButton.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Перед отправлением исправте ошибки валидации. Все поля должны быть заполнены.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

    }
}
