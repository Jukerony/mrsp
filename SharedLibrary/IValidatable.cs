﻿namespace SharedLibrary
{
    public interface IValidatable
    {
        /// <summary>
        /// Возвращает информацию о валидности элемента реализующего интерфейс
        /// </summary>
        /// <returns></returns>
        bool IsValid();
    }
}
