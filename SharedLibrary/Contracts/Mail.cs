﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace SharedLibrary.Contracts
{
    [DataContract]
    public class Mail:IValidatable,IDataErrorInfo
    {
        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public string Content { get; set; }

        private const string MailPattern = @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                                           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$";

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Content) ||
                                string.IsNullOrEmpty(From) ||
                                string.IsNullOrEmpty(Subject) ||
                                string.IsNullOrEmpty(To) ||
                                Subject.Length > 1024 ||
                                Content.Length > 4000 ||
                                From.Length > 256 ||
                                To.Length > 256 ||
                                !Regex.IsMatch(From, MailPattern) ||
                                !Regex.IsMatch(To, MailPattern)
            )
                return false;
            return true;
        }

        public string this[string columnName]
        {
            get
            {
                string error = "";
                switch (columnName)
                {
                    case "Subject":
                    {
                        error = SubjectValidation(error);
                        break;
                    }
                    case "Content":
                    {
                        error = ConentValidation(error);
                        break;
                    }
                    case "From":
                    {
                        error = FromValidation(error);
                        break;
                    }
                    case "To":
                    {
                        error = ToValidation(error);
                        break;
                    }
                }
                return error;
            }
        }

        private string ToValidation(string error)
        {
            if (string.IsNullOrEmpty(To))
            {
                error = "";
            }
            else if (To.Length > 256)
            {
                error = "Поле получатель должно быть не длиннее 256 символов";
            }
            else if (!Regex.IsMatch(To, MailPattern))
            {
                error = "Поле получатель должно быть в формате Email";
            }
            return error;
        }

        private string FromValidation(string error)
        {
            if (string.IsNullOrEmpty(From))
            {
                error = "";
            }
            else if (From.Length > 256)
            {
                error = "Поле отправитель должно быть не длиннее 256 символов";
            }
            else if (!Regex.IsMatch(From, MailPattern))
            {
                error = "Поле отправитель должно быть в формате Email";
            }
            return error;
        }

        private string ConentValidation(string error)
        {
            if (string.IsNullOrEmpty(Content))
            {
                error = "";
            }
            else if (Content.Length > 4000)
            {
                error = "Поле содержание является должно быть не длиннее 4000 символов.";
            }
            return error;
        }

        private string SubjectValidation(string error)
        {
            if (string.IsNullOrEmpty(Subject))
            {
                error = "";
            }
            else if (Subject.Length > 1024)
            {
                error = "Поле темы является должно быть не длиннее 1024 символов.";
            }
            return error;
        }

        public string Error { get; }
    }
}