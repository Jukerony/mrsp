﻿using System.Runtime.Serialization;

namespace SharedLibrary.Contracts
{
    /// <summary>
    /// Перечисление результатов регистрации письма
    /// </summary>
    [DataContract]
    public enum MailRegistrationResult
    {
        [EnumMember]
        Ok,
        [EnumMember]
        ValidationFaild,
        [EnumMember]
        DataBaseConnectFaild,
        [EnumMember]
        IncorectDbResult,
        [EnumMember]
        IncorrectFrom,
        [EnumMember]
        IncorrectTo,
        [EnumMember]
        IncorrectFromAndTo,
    }
}