﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace DataBaseAcsess.Helpers.DataBase
{
    public class DbMediator:IDisposable
    {
        protected readonly SqlConnection Connection;

        protected DbMediator()
        {
            var defaultConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            if(defaultConnectionString==null) throw new DataBaseConnectException("В приложении не задана строка подключения с названием DefaultConnection");
            Connection = new SqlConnection(defaultConnectionString.ConnectionString);
        }

        public void Dispose()
        {
            try
            {
                Connection.Close();
                Connection.Dispose();
            }
            catch (ObjectDisposedException)
            {
            }
        }
    }
}