﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using SharedLibrary.Contracts;

namespace DataBaseAcsess.Helpers.DataBase
{
    public class MailRegistrator:DbMediator
    {
        public async Task<MailRegistrationResult> RegistrateMail(Mail mail)
        {
            var cmd = new SqlCommand("sp_RegisterMail", Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            var retParam = InitCommandParameters(mail, cmd);
            try
            {
                await ExecuteCommand(cmd);
            }
            catch (Exception)
            {
                throw new DataBaseConnectException($"Ошибка при выполнении команды направленной на строку подключения {Connection.ConnectionString}");
            }
            long retValueLong;
            if(!long.TryParse(retParam.Value.ToString(),out retValueLong))
            {
                return MailRegistrationResult.IncorectDbResult;
            }
            switch (retValueLong)
            {
                case -3: return MailRegistrationResult.IncorrectFromAndTo;
                case -2: return MailRegistrationResult.IncorrectTo;
                case -1: return MailRegistrationResult.IncorrectFrom;
                default: return MailRegistrationResult.Ok;
            }
        }

        private async Task ExecuteCommand(SqlCommand cmd)
        {
            if (Connection.State != ConnectionState.Open)
            {
                await Connection.OpenAsync();
            }
            var reader = await cmd.ExecuteReaderAsync();
            reader.Close();
        }

        private static SqlParameter InitCommandParameters(Mail mail, SqlCommand cmd)
        {
            var retVal = cmd.Parameters.Add("RetVal", SqlDbType.Int);
            retVal.Direction = ParameterDirection.ReturnValue;
            var dateParam = new SqlParameter("@Date", SqlDbType.DateTime) {Value = DateTime.Now};
            var subjectParam = new SqlParameter("@Subject", SqlDbType.NVarChar, 1024) {Value = mail.Subject};
            var contentParam = new SqlParameter("@Content", SqlDbType.NVarChar, 4000) {Value = mail.Content};
            var fromParam = new SqlParameter("@From", SqlDbType.NVarChar, 256) {Value = mail.From};
            var toParam = new SqlParameter("@To", SqlDbType.NVarChar, 256) {Value = mail.To};
            cmd.Parameters.Add(dateParam);
            cmd.Parameters.Add(subjectParam);
            cmd.Parameters.Add(contentParam);
            cmd.Parameters.Add(fromParam);
            cmd.Parameters.Add(toParam);
            return retVal;
        }
    }
}