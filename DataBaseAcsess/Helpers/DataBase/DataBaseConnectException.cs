﻿using System;

namespace DataBaseAcsess.Helpers.DataBase
{
    public class DataBaseConnectException:Exception
    {
        public override string Message => "Ошибка при установлении подключения с базой данных";

        public DataBaseConnectException(string msg)
            :base(msg)
        {
        }
    }
}