﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using DataBaseAcsess.Helpers.DataBase;
using SharedLibrary.Contracts;

namespace DataBaseAcsess
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerSession
    )]
    public class DbAcsessService : IDbAcsessService
    {
        public async Task<MailRegistrationResult> RegisterMail(Mail mail)
        {
            if(mail==null || !mail.IsValid())
            {
                return MailRegistrationResult.ValidationFaild;
            }
            try
            {
                using (var mailRegistrator = new MailRegistrator())
                {
                    return await mailRegistrator.RegistrateMail(mail);
                }
            }
            catch (DataBaseConnectException)
            {
                return MailRegistrationResult.DataBaseConnectFaild;
            }
        }
    }
}
