﻿using System.ServiceModel;
using System.Threading.Tasks;
using SharedLibrary.Contracts;

namespace DataBaseAcsess
{
    [ServiceContract]
    public interface IDbAcsessService
    {
        [OperationContract]
        Task<MailRegistrationResult> RegisterMail(Mail mail);
    }

}
